package com.diegoferreiracaetano.data.remote.city

import com.diegoferreiracaetano.data.remote.OpenWeatherApi
import com.diegoferreiracaetano.data.remote.weather.ItemsEntity
import com.diegoferreiracaetano.data.remote.weather.TempEntity
import com.diegoferreiracaetano.data.remote.weather.WeatherEntity
import com.diegoferreiracaetano.data.remote.weather.transform
import com.diegoferreiracaetano.data.remote.weather.transformcity
import com.diegoferreiracaetano.domain.location.Location
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class CityRepositoryRemoteTest {

    private val service = mockk<OpenWeatherApi>()
    private lateinit var repositoryRemote: CityRepositoryRemote

    @Before
    fun setup() {
        repositoryRemote =
            CityRepositoryRemote(
                service
            )
    }

    @Test
    fun `Given service, When calling cities, Then assert values`() {

        runBlocking {

            coEvery { service.cities(any(), any(), any(), any(), any()) } returns flowOf(expected)

            val actual = repositoryRemote.cities(Location(51.1, 51.1)).single()

            assertEquals(actual, expected.cities.transformcity())
        }
    }

    @Test
    fun `Given service, When calling city, Then assert values`() {

        runBlocking {

            coEvery { service.city(any(), any(), any()) } returns flowOf(expected)

            val actual = repositoryRemote.city(1).single()

            assertEquals(actual, city.transform())
        }
    }

    private val city = CityEntity(
        id = 1,
        name = "",
        temp = TempEntity(
            51.1,
            51.1,
            51.1
        ),
        weather = listOf(
            WeatherEntity(
                id = 1,
                main = "",
                description = "",
                icon = ""
            )
        )
    )

    private val expected = ItemsEntity(cities = listOf(city))
}
