package com.diegoferreiracaetano.data.location

import android.content.Context
import android.location.Location
import android.location.LocationManager
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.diegoferreiracaetano.domain.location.Location as LocationInternal
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf

@RunWith(AndroidJUnit4::class)
class LocationRepositoryImpTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private val sls = shadowOf(locationManager)

    private lateinit var repository: LocationRepositoryImp

    @Before
    fun setUp() {
        repository = LocationRepositoryImp(locationManager)
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @ExperimentalCoroutinesApi
    @Test(timeout = 5_000L)
    fun `Given locationManager When call repository location Should location`() = runBlockingTest {

        locationManager.removeTestProvider(LocationManager.NETWORK_PROVIDER)

        val job = repository.location().onEach {
            assertEquals(it, LocationInternal(10.0, 20.0))
        }.launchIn(this)

        sls.simulateLocation(mock())

        job.cancel()
    }

    private fun mock(): Location {

        val location = Location(LocationManager.NETWORK_PROVIDER)
        location.latitude = 10.0
        location.longitude = 20.0
        location.accuracy = 500f
        location.time = System.currentTimeMillis()
        location.elapsedRealtimeNanos = System.nanoTime()
        return location
    }
}
