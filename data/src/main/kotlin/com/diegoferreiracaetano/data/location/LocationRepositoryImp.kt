package com.diegoferreiracaetano.data.location

import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import com.diegoferreiracaetano.domain.location.LocationRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow

internal class LocationRepositoryImp(
    private val locationManager: LocationManager
) : LocationRepository {

    @ExperimentalCoroutinesApi
    override fun location() = callbackFlow {

        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, object : LocationListener {
                override fun onLocationChanged(location: Location) {
                    offer(location.transform())
                    locationManager.removeUpdates(this)
                }
                override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
                override fun onProviderEnabled(provider: String) {}
                override fun onProviderDisabled(provider: String) {}
            })
        } catch (e: SecurityException) {
            close(e)
        }

        awaitClose { cancel() }
    }
}
