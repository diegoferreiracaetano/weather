package com.diegoferreiracaetano.data.location

import android.location.Location
import com.diegoferreiracaetano.domain.location.Location as LocationInternal

internal fun Location.transform() = LocationInternal(latitude, longitude)
