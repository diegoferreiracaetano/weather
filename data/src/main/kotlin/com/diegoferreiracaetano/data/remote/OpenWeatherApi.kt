package com.diegoferreiracaetano.data.remote

import com.diegoferreiracaetano.data.remote.weather.ItemsEntity
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Query

internal interface OpenWeatherApi {

    @GET("/data/2.5/find")
    fun cities(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("cnt") limit: Int,
        @Query("APPID") api: String,
        @Query("units") units: String = "metric",
        @Query("lang") lang: String = "pt_br"
    ): Flow<ItemsEntity>

    @GET("/data/2.5/group")
    fun city(
        @Query("id") id: Int,
        @Query("APPID") api: String,
        @Query("units") units: String = "metric",
        @Query("lang") lang: String = "pt_br"
    ): Flow<ItemsEntity>
}
