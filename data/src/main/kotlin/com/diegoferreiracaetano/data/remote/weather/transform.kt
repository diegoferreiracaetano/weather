package com.diegoferreiracaetano.data.remote.weather

import com.diegoferreiracaetano.data.BuildConfig.END_POINT
import com.diegoferreiracaetano.data.remote.city.CityEntity
import com.diegoferreiracaetano.domain.city.City
import com.diegoferreiracaetano.domain.temp.Temp
import com.diegoferreiracaetano.domain.weather.Weather

internal fun TempEntity.transform() = Temp(temp, min, max)

internal fun WeatherEntity.transform() = Weather(id, main, description, "${END_POINT.replace("api.", "")}/img/wn/$icon@2x.png")

internal fun List<WeatherEntity>.transform() = map { it.transform() }

internal fun CityEntity.transform() = City(id, name, temp.transform(), weather.transform())

internal fun List<CityEntity>.transformcity() = map { it.transform() }
