package com.diegoferreiracaetano.data.remote.weather

import com.google.gson.annotations.SerializedName

internal class TempEntity(
    @SerializedName("temp") val temp: Double,
    @SerializedName("temp_min") val min: Double,
    @SerializedName("temp_max") val max: Double
)
