package com.diegoferreiracaetano.data.remote.city

import com.diegoferreiracaetano.data.remote.weather.TempEntity
import com.diegoferreiracaetano.data.remote.weather.WeatherEntity
import com.google.gson.annotations.SerializedName

internal class CityEntity(
    val id: Int,
    val name: String,
    @SerializedName("main") val temp: TempEntity,
    val weather: List<WeatherEntity>
)
