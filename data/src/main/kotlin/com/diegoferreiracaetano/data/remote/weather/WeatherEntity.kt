package com.diegoferreiracaetano.data.remote.weather

internal class WeatherEntity(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)
