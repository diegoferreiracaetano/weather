package com.diegoferreiracaetano.data.remote.city

import com.diegoferreiracaetano.data.BuildConfig.OPEN_WEATHER_API
import com.diegoferreiracaetano.data.remote.OpenWeatherApi
import com.diegoferreiracaetano.data.remote.weather.transform
import com.diegoferreiracaetano.data.remote.weather.transformcity
import com.diegoferreiracaetano.domain.city.CityRepository
import com.diegoferreiracaetano.domain.location.Location
import kotlinx.coroutines.flow.map

internal class CityRepositoryRemote(private val api: OpenWeatherApi) :
    CityRepository {

    override fun cities(location: Location) = api
        .cities(
            location.latitude,
            location.longitude,
            LIMIT,
            OPEN_WEATHER_API
        )
        .map {
            it.cities.transformcity()
        }

    override fun city(id: Int) = api.city(id, OPEN_WEATHER_API).map {
        it.cities.first().transform()
    }

    companion object {
        const val LIMIT = 15
    }
}
