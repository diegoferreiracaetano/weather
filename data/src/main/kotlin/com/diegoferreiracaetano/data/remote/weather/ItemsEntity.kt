package com.diegoferreiracaetano.data.remote.weather

import com.diegoferreiracaetano.data.remote.city.CityEntity
import com.google.gson.annotations.SerializedName

internal class ItemsEntity(
    @SerializedName("list") val cities: List<CityEntity>
)
