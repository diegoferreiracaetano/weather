#!/usr/bin/env bash

major_version="$( grep MAJOR_VERSION gradle.properties | cut -d '=' -f2 )"
minor_version="$( grep MINOR_VERSION gradle.properties | cut -d '=' -f2 )"
patch_version="$( grep PATCH_VERSION gradle.properties | cut -d '=' -f2 )"
tag="$major_version.$minor_version.$patch_version"
version_code="$( grep VERSION_CODE gradle.properties | cut -d '=' -f2 )"

echo $tag
