curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $1" \
     --data '{ "name": "Release '$2'", "tag_name": "'$2'", "description": "Release '$2'", "ref": "'$3'" }' \
     --request POST https://gitlab.com/api/v4/projects/16815626/releases