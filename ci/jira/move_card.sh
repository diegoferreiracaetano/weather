#!/usr/bin/env bash

curl --request POST \
  --url "https://diegoferreiracaetano.atlassian.net/rest/api/2/issue/$1/transitions" \
  --user "$2" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '{  "transition": { "id": "'$3'" }}'