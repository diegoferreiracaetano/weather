#!/usr/bin/env bash

msg=""

case $3
  in 11) msg=$(printf "Tarefa está adapta para desenvolvimento");;
     21) msg=$(printf "Tarefa está em desenvolvimento");;
     31) msg=$(printf "Tarefa foi concluida");;
  *) msg="";;
esac

./ci/jira/comment.sh $1 $2 "$msg"