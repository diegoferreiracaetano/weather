#!/usr/bin/env bash

curl --request POST \
  --url "https://diegoferreiracaetano.atlassian.net/rest/api/2/issue/$1/comment" \
  --user "$2" \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data "{\"body\": \"$3\" }"