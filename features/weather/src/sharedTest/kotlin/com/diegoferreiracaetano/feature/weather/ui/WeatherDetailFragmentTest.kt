package com.diegoferreiracaetano.feature.weather.ui

import android.nfc.NfcAdapter.EXTRA_ID
import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.diegoferreiracaetano.app.R
import com.diegoferreiracaetano.domain.city.City
import com.diegoferreiracaetano.domain.temp.Temp
import com.diegoferreiracaetano.domain.weather.Weather
import com.diegoferreiracaetano.toLiveDataResultTest
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.mockito.ArgumentMatchers.anyInt

@RunWith(AndroidJUnit4::class)
class WeatherDetailFragmentTest : AutoCloseKoinTest() {

    private val viewModel = mockk<WeatherViewModel>()

    private val fragmentArgs = Bundle().apply {
        putInt(EXTRA_ID, anyInt())
    }

    @Before
    fun before() {
        startKoin {}

        loadKoinModules(module(override = true) {
            single { viewModel }
        })

        coEvery { viewModel.city(anyInt()) } returns city.toLiveDataResultTest()

        launchFragmentInContainer<WeatherDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = fragmentArgs
        )
    }

    @Test
    fun givenStartScreen_shouldTextCity() {

        onView(withId(R.id.detail_txt_city))
            .check(matches(withText("Roma")))
    }

    @Test
    fun givenStartScreen_shouldTextDescription() {

        onView(withId(R.id.detail_txt_description))
            .check(matches(withText("Chuva")))
    }

    @Test
    fun givenStartScreen_shouldTextTemp() {

        onView(withId(R.id.detail_txt_temp))
            .check(matches(withText("25")))
    }

    @Test
    fun givenStartScreen_shouldTextTempMin() {

        onView(withId(R.id.detail_txt_min))
            .check(matches(withText("21")))
    }

    @Test
    fun givenStartScreen_shouldTextTempMax() {

        onView(withId(R.id.detail_txt_max))
            .check(matches(withText("29")))
    }

    private val city = City(
        id = 1,
        name = "Roma",
        temp = Temp(
            25.1,
            21.1,
            29.1
        ),
        weather = listOf(
            Weather(
                id = 1,
                main = "",
                description = "Chuva",
                icon = null
            )
        )
    )
}
