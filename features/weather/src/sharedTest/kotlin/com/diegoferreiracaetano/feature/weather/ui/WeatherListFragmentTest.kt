package com.diegoferreiracaetano.feature.weather.ui

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.diegoferreiracaetano.app.R
import com.diegoferreiracaetano.domain.city.City
import com.diegoferreiracaetano.domain.temp.Temp
import com.diegoferreiracaetano.domain.weather.Weather
import com.diegoferreiracaetano.feature.weather.ui.WeatherListFragment.Companion.EXTRA_LAT
import com.diegoferreiracaetano.feature.weather.ui.WeatherListFragment.Companion.EXTRA_LON
import com.diegoferreiracaetano.router.weather.WeatherListRouter
import com.diegoferreiracaetano.toLiveDataResultTest
import io.mockk.coEvery
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest

@RunWith(AndroidJUnit4::class)
class WeatherListFragmentTest : AutoCloseKoinTest() {

    private val viewModel = mockk<WeatherViewModel>()

    private val fragmentArgs = Bundle().apply {
        putString(EXTRA_LAT, "1.1")
        putString(EXTRA_LON, "1.1")
    }

    @Before
    fun before() {
        startKoin {}

        loadKoinModules(module(override = true) {
            single { viewModel }
        })

        coEvery { viewModel.cities(1.1, 1.1) } returns cities.toLiveDataResultTest(WeatherListRouter())

        launchFragmentInContainer<WeatherListFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = fragmentArgs
        )
    }

    @Test
    fun givenStartScreen_shouldTextCity() {

        onView(withId(R.id.weather_recycle))
            .check(matches(hasDescendant(withText("Roma"))))
    }

    @Test
    fun givenStartScreen_shouldTextDescription() {

        onView(withId(R.id.weather_recycle))
            .check(matches(hasDescendant(withText("Chuva"))))
    }

    @Test
    fun givenStartScreen_shouldTextTemp() {

        onView(withId(R.id.weather_recycle))
            .check(matches(hasDescendant(withText("25"))))
    }

    @Test
    fun givenStartScreen_shouldTextTempMin() {

        onView(withId(R.id.weather_recycle))
            .check(matches(hasDescendant(withText("21"))))
    }

    @Test
    fun givenStartScreen_shouldTextTempMax() {

        onView(withId(R.id.weather_recycle))
            .check(matches(hasDescendant(withText("29"))))
    }

    private val city = City(
        id = 1,
        name = "Roma",
        temp = Temp(
            25.1,
            21.1,
            29.1
        ),
        weather = listOf(
            Weather(
                id = 1,
                main = "",
                description = "Chuva",
                icon = null
            )
        )
    )

    private val cities = listOf(city)
}
