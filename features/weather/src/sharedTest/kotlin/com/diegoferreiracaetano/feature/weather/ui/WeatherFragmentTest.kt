package com.diegoferreiracaetano.feature.weather.ui

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Application
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.diegoferreiracaetano.app.R
import com.diegoferreiracaetano.domain.location.Location
import com.diegoferreiracaetano.toLiveDataResultTest
import com.google.android.gms.maps.SupportMapFragment
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.robolectric.Shadows
import org.robolectric.shadows.ShadowApplication

@RunWith(AndroidJUnit4::class)
class WeatherFragmentTest : AutoCloseKoinTest() {

    private val viewModel = mockk<WeatherViewModel>()
    private lateinit var application: Application
    private lateinit var app: ShadowApplication
    private lateinit var fragment: FragmentScenario<WeatherFragment>

    @Before
    fun before() {
        application = ApplicationProvider.getApplicationContext()
        app = Shadows.shadowOf(application)
        app.grantPermissions(ACCESS_FINE_LOCATION)

        every { viewModel.location() } returns location.toLiveDataResultTest()

        startKoin {}

        loadKoinModules(module(override = true) {
            single { viewModel }
        })

        fragment = launchFragmentInContainer<WeatherFragment>(
            themeResId = R.style.AppTheme
        )
    }

    @Test
    fun givenStartScreen_verifyMapIsVisible() {

        fragment.onFragment {

            val mapFragment = it.childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

            mapFragment.getMapAsync {

                onView(withId(R.id.map)).check(matches(isDisplayed()))
            }
        }
    }

    @Test
    fun givenStartScreen_verifyButtonIsVisible() {

        fragment.onFragment {

            val mapFragment = it.childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

            mapFragment.getMapAsync {

                onView(withId(R.id.btn_search)).check(matches(isDisplayed()))
            }
        }
    }

    private val location = Location(10.0, 20.0)
}
