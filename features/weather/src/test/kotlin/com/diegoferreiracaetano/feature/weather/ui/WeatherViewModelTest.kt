package com.diegoferreiracaetano.feature.weather.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.diegoferreiracaetano.domain.ResultRouter
import com.diegoferreiracaetano.domain.city.CitiesInteractor
import com.diegoferreiracaetano.domain.city.City
import com.diegoferreiracaetano.domain.city.FindCityInteractor
import com.diegoferreiracaetano.domain.location.Location
import com.diegoferreiracaetano.domain.location.LocationInteractor
import com.diegoferreiracaetano.domain.temp.Temp
import com.diegoferreiracaetano.domain.weather.Weather
import com.diegoferreiracaetano.router.weather.WeatherListRouter
import com.diegoferreiracaetano.toResultSuccessTest
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
internal class WeatherViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val citiesInteractor = mockk<CitiesInteractor>()
    private val findInteractor = mockk<FindCityInteractor>()
    private val locationInteractor = mockk<LocationInteractor>()
    private val router = mockk<WeatherListRouter>()

    private lateinit var viewModel: WeatherViewModel

    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        viewModel = WeatherViewModel(
            citiesInteractor,
            findInteractor,
            locationInteractor,
            router
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `Given interactor cities When call cities Then verify result success`() = runBlocking {

        val observer = mockk<Observer<Result<ResultRouter<List<City>>>>>()
        val result = cities.toResultSuccessTest(mockk())

        coEvery { citiesInteractor.invoke(any()) } returns result

        viewModel.cities(1.0, 1.0).observeForever(observer)

        coVerify { observer.onChanged(result.single()) }
    }

    @Test(expected = Throwable::class)
    fun `Given interactor cities When call cities Then verify result error`() {

        val observer = mockk<Observer<Result<ResultRouter<List<City>>>>>()
        val result = Throwable("error")

        coEvery { citiesInteractor(mockk()) } throws result
        viewModel.cities(1.0, 1.0).observeForever(observer)

        verify { observer.onChanged(Result.failure(result)) }
    }

    @Test
    fun `Given interactor city When call city Then verify result success`() {

        val observer = mockk<Observer<Result<City>>>()
        val result = city.toResultSuccessTest()

        coEvery { findInteractor(1) } returns result
        viewModel.city(1).observeForever(observer)

        coVerify { observer.onChanged(result.single()) }
    }

    @Test(expected = Throwable::class)
    fun `Given interactor city When call city Then verify result error`() {

        val observer = mockk<Observer<Result<City>>>()
        val result = Throwable("error")

        coEvery { findInteractor(1) } throws result
        viewModel.city(1).observeForever(observer)

        verify { observer.onChanged(Result.failure(result)) }
    }

    @Test
    fun `Given interactor location When call location Then verify result success`() {

        val observer = mockk<Observer<Result<Location>>>()
        val result = location.toResultSuccessTest()

        coEvery { locationInteractor(Unit, Dispatchers.Main) } returns result
        viewModel.location().observeForever(observer)

        coVerify { observer.onChanged(result.single()) }
    }

    @Test(expected = Throwable::class)
    fun `Given interactor location When call location Then verify result error`() {

        val observer = mockk<Observer<Result<Location>>>()
        val result = Throwable("error")

        coEvery { locationInteractor(Unit) } throws result
        viewModel.location().observeForever(observer)

        verify { observer.onChanged(Result.failure(result)) }
    }

    @Test
    fun `When router Then verify result`() {

        assert(viewModel.router() is WeatherListRouter)
    }

    private val location = Location(10.0, 20.0)

    private val city = City(
        id = 1,
        name = "city",
        temp = Temp(
            51.1,
            51.1,
            51.1
        ),
        weather = listOf(
            Weather(
                id = 1,
                main = "",
                description = "",
                icon = null
            )
        )
    )

    private val cities = listOf(city)
}
