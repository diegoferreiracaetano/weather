package com.diegoferreiracaetano.feature.weather.ui

import android.Manifest
import android.content.pm.PackageManager
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.diegoferreiracaetano.app.R
import com.google.android.material.snackbar.Snackbar

private const val REQUEST_PERMISSIONS_REQUEST_CODE = 123

fun Fragment.checkPermissions(): Boolean {
    val permissionState = ContextCompat.checkSelfPermission(
        requireContext(),
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    return permissionState == PackageManager.PERMISSION_GRANTED
}

fun Fragment.requestPermissions() {
    val shouldProvideRationale = shouldShowRequestPermissionRationale(
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    if (shouldProvideRationale) {

        showSnackbar(R.string.permission_rationale, android.R.string.ok,
            View.OnClickListener {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE
                )
            })
    } else {

        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }
}

fun Fragment.showSnackbar(
    mainTextStringId: Int,
    actionStringId: Int,
    listener: View.OnClickListener
) {
    Snackbar.make(requireView(), getString(mainTextStringId),
        Snackbar.LENGTH_INDEFINITE)
        .setAction(getString(actionStringId), listener)
        .show()
}
