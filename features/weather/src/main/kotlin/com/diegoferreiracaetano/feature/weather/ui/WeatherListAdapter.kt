package com.diegoferreiracaetano.feature.weather.ui
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.diegoferreiracaetano.app.R
import com.diegoferreiracaetano.commons.setImageUrl
import com.diegoferreiracaetano.domain.city.City
import kotlin.math.roundToInt
import kotlinx.android.synthetic.main.item_weather.view.weather_img
import kotlinx.android.synthetic.main.item_weather.view.weather_txt_ciry
import kotlinx.android.synthetic.main.item_weather.view.weather_txt_description
import kotlinx.android.synthetic.main.item_weather.view.weather_txt_max
import kotlinx.android.synthetic.main.item_weather.view.weather_txt_min
import kotlinx.android.synthetic.main.item_weather.view.weather_txt_temp

internal class WeatherListAdapter(
    private var items: List<City>
) : RecyclerView.Adapter<WeatherListAdapter.ViewHolder>() {

    var onItemClick: ((City) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_weather, parent, false)

        return ViewHolder(
            itemView
        )
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val city = items[position]
        holder.temp.text = city.temp.temp.roundToInt().toString()
        holder.min.text = city.temp.min.roundToInt().toString()
        holder.max.text = city.temp.max.roundToInt().toString()
        holder.city.text = city.name
        city.weather.forEach {
            holder.description.text = it.description
            holder.icon.setImageUrl(it.icon)
        }
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(city)
        }
    }

    internal class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val icon: ImageView = view.weather_img
        val city: TextView = view.weather_txt_ciry
        val description: TextView = view.weather_txt_description
        val temp: TextView = view.weather_txt_temp
        val min: TextView = view.weather_txt_min
        val max: TextView = view.weather_txt_max
    }
}
