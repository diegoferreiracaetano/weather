package com.diegoferreiracaetano.feature.weather.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.diegoferreiracaetano.domain.city.CitiesInteractor
import com.diegoferreiracaetano.domain.city.FindCityInteractor
import com.diegoferreiracaetano.domain.location.Location
import com.diegoferreiracaetano.domain.location.LocationInteractor
import com.diegoferreiracaetano.router.Router
import kotlinx.coroutines.Dispatchers

internal class WeatherViewModel(
    private val weatherInteractor: CitiesInteractor,
    private val findCityInteractor: FindCityInteractor,
    private val locationInteractor: LocationInteractor,
    private val router: Router
) : ViewModel() {

    fun cities(lat: Double, lon: Double) = weatherInteractor(
        Location(
            lat,
            lon
        )
    ).asLiveData()

    fun city(id: Int) = findCityInteractor(id).asLiveData()

    fun location() = locationInteractor(Unit, Dispatchers.Main).asLiveData()

    fun router() = router
}
