package com.diegoferreiracaetano.feature.weather.ui

import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import androidx.appcompat.widget.SearchView.GONE
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.diegoferreiracaetano.app.R
import com.diegoferreiracaetano.commons.setImageUrl
import com.diegoferreiracaetano.commons.showError
import com.diegoferreiracaetano.domain.city.City
import kotlin.math.roundToInt
import kotlinx.android.synthetic.main.fragment_detail_weather.detail_group
import kotlinx.android.synthetic.main.fragment_detail_weather.detail_img_temp
import kotlinx.android.synthetic.main.fragment_detail_weather.detail_txt_city
import kotlinx.android.synthetic.main.fragment_detail_weather.detail_txt_description
import kotlinx.android.synthetic.main.fragment_detail_weather.detail_txt_max
import kotlinx.android.synthetic.main.fragment_detail_weather.detail_txt_min
import kotlinx.android.synthetic.main.fragment_detail_weather.detail_txt_temp
import kotlinx.android.synthetic.main.weather_detail_placeholder.shimmer_view_detail
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherDetailFragment : Fragment(R.layout.fragment_detail_weather) {

    private val viewModel: WeatherViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startShimmer()
        setupWeather()
    }

    override fun onStop() {
        super.onStop()
        stopShimmer()
    }

    private fun setupWeather() {
        requireArguments().apply {
            val id = getInt(EXTRA_ID)
            viewModel.city(id).observe(viewLifecycleOwner, Observer {
                it.onSuccess(::showWeather)
                    .onFailure {
                        showError(R.string.Weathers_msg_error, it)
                    } })
        }
    }

    private fun startShimmer() {
        shimmer_view_detail.startShimmer()
    }

    private fun stopShimmer() {
        shimmer_view_detail.visibility = GONE
        shimmer_view_detail.stopShimmer()
        detail_group.visibility = VISIBLE
    }

    private fun showWeather(city: City) {
        stopShimmer()

        detail_txt_temp.text = city.temp.temp.roundToInt().toString()
        detail_txt_min.text = city.temp.min.roundToInt().toString()
        detail_txt_max.text = city.temp.max.roundToInt().toString()
        detail_txt_city.text = city.name
        city.weather.forEach {
            detail_txt_description.text = it.description
            detail_img_temp.setImageUrl(it.icon)
        }
    }

    companion object {
        const val EXTRA_ID = "id"
    }
}
