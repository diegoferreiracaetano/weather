package com.diegoferreiracaetano.feature.weather.ui

import android.os.Bundle
import android.view.View
import android.view.View.VISIBLE
import androidx.appcompat.widget.SearchView.GONE
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.diegoferreiracaetano.app.R
import com.diegoferreiracaetano.commons.navigate
import com.diegoferreiracaetano.commons.showError
import com.diegoferreiracaetano.domain.ResultRouter
import com.diegoferreiracaetano.domain.city.City
import kotlinx.android.synthetic.main.fragment_weather_list.weather_group
import kotlinx.android.synthetic.main.fragment_weather_list.weather_recycle
import kotlinx.android.synthetic.main.fragment_weather_loading.shimmer_view_container
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherListFragment : Fragment(R.layout.fragment_weather_list) {

    private val viewModel: WeatherViewModel by viewModel()
    private lateinit var weatherAdapter: WeatherListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startShimmer()
        setupWeather()
    }

    override fun onStop() {
        super.onStop()
        stopShimmer()
    }

    private fun setupWeather() {
        requireArguments().apply {
            val lat = getString(EXTRA_LAT)
            val lon = getString(EXTRA_LON)
            viewModel.cities(lat!!.toDouble(), lon!!.toDouble()).observe(viewLifecycleOwner, Observer {
                it.onSuccess(::showweather)
                    .onFailure {
                        showError(R.string.location_error, it)
                    } })
        }
    }

    private fun startShimmer() {
        shimmer_view_container.startShimmer()
    }

    private fun stopShimmer() {
        weather_group.visibility = VISIBLE
        shimmer_view_container.visibility = GONE
        shimmer_view_container.stopShimmer()
    }

    private fun showweather(result: ResultRouter<List<City>>) {
        stopShimmer()
        weatherAdapter =
            WeatherListAdapter(result.result)
        weather_recycle.adapter = weatherAdapter
        weatherAdapter.onItemClick = {
            navigate(result.router, it.id)
        }
    }

    companion object {
        const val EXTRA_LAT = "lat"
        const val EXTRA_LON = "lon"
    }
}
