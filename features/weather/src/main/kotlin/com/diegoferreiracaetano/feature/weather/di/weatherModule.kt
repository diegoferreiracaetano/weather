package com.diegoferreiracaetano.feature.weather.di

import com.diegoferreiracaetano.feature.weather.ui.WeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

val weatherModule: Module = module {
    viewModel {
        WeatherViewModel(
            get(),
            get(),
            get(),
            get(named("list"))
        )
    }
}
