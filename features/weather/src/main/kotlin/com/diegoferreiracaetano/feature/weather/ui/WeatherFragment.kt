package com.diegoferreiracaetano.feature.weather.ui

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.diegoferreiracaetano.app.BuildConfig
import com.diegoferreiracaetano.app.R
import com.diegoferreiracaetano.commons.navigate
import com.diegoferreiracaetano.commons.query
import com.diegoferreiracaetano.commons.showError
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_weather.btn_search
import kotlinx.android.synthetic.main.fragment_weather.group_map
import kotlinx.android.synthetic.main.fragment_weather_placehouder.shimmer_view_map
import org.koin.androidx.viewmodel.ext.android.viewModel

class WeatherFragment : Fragment(R.layout.fragment_weather) {

    private val viewModel: WeatherViewModel by viewModel()

    private var position: LatLng? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startShimmer()
        setupCities()

        if (!checkPermissions()) {
            requestPermissions()
        } else {
            setupLocation()
        }
    }

    override fun onStop() {
        super.onStop()
        stopShimmer()
    }

    private fun startShimmer() {
        shimmer_view_map.startShimmer()
    }

    private fun stopShimmer() {
        group_map.visibility = View.VISIBLE
        shimmer_view_map.visibility = View.GONE
        shimmer_view_map.stopShimmer()
    }

    private fun setupLocation() {

        if (position == null) {
            viewModel.location().observe(viewLifecycleOwner, Observer {
                it.onSuccess {
                    position = LatLng(it.latitude, it.longitude)
                    setupMap()
                }.onFailure {
                    stopShimmer()
                    showError(R.string.location_error, it)
                }
            })
        } else {
            setupMap()
        }
    }

    private fun setupMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment

        mapFragment?.let {
            it.getMapAsync { maps ->
                stopShimmer()
                maps.clear()
                position?.let {
                    maps.moveCamera(CameraUpdateFactory.newLatLngZoom(it,
                        ZOOM
                    ))
                    maps.addMarker(MarkerOptions().position(it))
                    maps.setOnMapClickListener {
                        position = LatLng(it.latitude, it.longitude).also {
                            maps.clear()
                            maps.addMarker(MarkerOptions().position(it))
                            maps.moveCamera(CameraUpdateFactory.newLatLng(it))
                        }
                    }
                }
            }
        }
    }

    private fun setupCities() {
        btn_search.setOnClickListener {
            position?.let {

                val param = mapOf(
                    WeatherListFragment.EXTRA_LAT to it.latitude,
                    WeatherListFragment.EXTRA_LON to it.longitude
                ).query()

                navigate(viewModel.router(), param)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (requestCode != REQUEST_PERMISSIONS_REQUEST_CODE) return

        when (PackageManager.PERMISSION_GRANTED) {
            grantResults[0] -> setupLocation()
            else -> showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                View.OnClickListener {
                    val intent = Intent().apply {
                        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        data = Uri.fromParts("package", BuildConfig.LIBRARY_PACKAGE_NAME, null)
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    }
                    startActivity(intent)
                })
        }
    }

    private companion object {
        const val ZOOM = 10f
        const val REQUEST_PERMISSIONS_REQUEST_CODE = 123
    }
}
