package com.diegoferreiracaetano.router.weather

import com.diegoferreiracaetano.router.Router

class DetailRouter : Router {

    override fun navigate(any: Any) = "android-app://weather7id=$any"
    override fun isStart(): Boolean = false
}
