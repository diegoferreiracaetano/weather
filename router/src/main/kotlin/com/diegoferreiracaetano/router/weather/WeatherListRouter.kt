package com.diegoferreiracaetano.router.weather

import com.diegoferreiracaetano.router.Router

class WeatherListRouter : Router {

    override fun navigate(any: Any) = "android-app://feature.weather$any"
    override fun isStart(): Boolean = false
}
