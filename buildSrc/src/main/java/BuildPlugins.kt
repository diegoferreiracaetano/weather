import dependencies.Versions

object BuildPlugins {
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.KOTLIN}"
    const val GRADLE = "com.android.tools.build:gradle:${Versions.GRADLE}"
    const val NAVIGATION = "android.arch.navigation:navigation-safe-args-gradle-plugin:${Versions.NAVIGATION_SAFE_ARGS}"
    const val JACOCO = "org.jacoco:org.jacoco.core:${Versions.JACOCO}"
    const val SONAR =  "org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.7"
    const val FIREBASE_APP_DISTRIBUTION = "com.google.firebase:firebase-appdistribution-gradle:1.1.0"
    const val GOOGLE_SERVICE = "com.google.gms:google-services:4.3.3"
}
