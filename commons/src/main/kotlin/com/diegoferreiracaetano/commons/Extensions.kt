package com.diegoferreiracaetano.commons

import android.net.Uri
import android.widget.ImageView
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.diegoferreiracaetano.router.Router
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

fun ImageView.setImageUrl(url: String?) {
    if (!url.isNullOrEmpty()) {
        Glide.with(context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .skipMemoryCache(true)
            .into(this)
    }
}

fun Fragment.navigate(router: Router, any: Any) {

    val url = Uri.parse(router.navigate(any))

    if (router.isStart()) {
        val options = NavOptions.Builder()
            .setPopUpTo(findNavController().graph.startDestination, true)
            .build()
        findNavController().navigate(url, options)
    } else
        findNavController().navigate(url)
}

fun Map<String, Any>.query(): String {
    val builder = Uri.Builder()
    forEach { key, value ->

        builder
            .appendQueryParameter(key, value.toString())
    }

    return builder.build().toString()
}

fun Fragment.showError(@StringRes int: Int, t: Throwable) {
    Timber.e(t)
    requireActivity().runOnUiThread {
        Snackbar.make(requireView(), int, Snackbar.LENGTH_LONG).show()
    }
}
