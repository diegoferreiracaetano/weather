package com.diegoferreiracaetano.weather

import android.app.Application
import com.diegoferreiracaetano.data.di.dataModule
import com.diegoferreiracaetano.domain.di.domainModule
import com.diegoferreiracaetano.feature.weather.di.weatherModule
import com.diegoferreiracaetano.router.di.routerModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MainApplication)
            loadKoinModules(listOf(
                weatherModule,
                domainModule,
                dataModule,
                routerModule
            ))
        }
    }
}
