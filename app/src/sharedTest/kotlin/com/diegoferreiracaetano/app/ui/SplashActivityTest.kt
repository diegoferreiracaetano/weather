package com.diegoferreiracaetano.app.ui

import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.diegoferreiracaetano.weather.R
import com.diegoferreiracaetano.weather.ui.SplashActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.AutoCloseKoinTest

@RunWith(AndroidJUnit4::class)
class SplashActivityTest : AutoCloseKoinTest() {

    private lateinit var scenario: ActivityScenario<SplashActivity>

    @Test
    fun whenInitScreen_verifyImageAnsTitleIsVisible() {
        runBlocking(Dispatchers.Main) {
            scenario = launch(SplashActivity::class.java)

            scenario.recreate()

            onView(withId(R.id.splash_img)).check(matches(isDisplayed()))
            onView(withId(R.id.splash_title)).check(matches(isDisplayed()))
        }
    }
}
