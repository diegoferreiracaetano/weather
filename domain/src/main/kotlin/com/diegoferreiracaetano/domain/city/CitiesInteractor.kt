package com.diegoferreiracaetano.domain.city

import com.diegoferreiracaetano.domain.Interactor
import com.diegoferreiracaetano.domain.ResultRouter
import com.diegoferreiracaetano.domain.location.Location
import com.diegoferreiracaetano.router.Router
import kotlinx.coroutines.flow.map

class CitiesInteractor(
    private val repository: CityRepository,
    private val router: Router
) : Interactor<Location, ResultRouter<List<City>>>() {

    override fun execute(parameters: Location) = repository.cities(
        parameters
    ).map {
        ResultRouter.add(it, router)
    }
}
