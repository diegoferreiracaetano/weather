package com.diegoferreiracaetano.domain.city

import com.diegoferreiracaetano.domain.temp.Temp
import com.diegoferreiracaetano.domain.weather.Weather

data class City(
    val id: Int,
    val name: String,
    val temp: Temp,
    val weather: List<Weather>
)
