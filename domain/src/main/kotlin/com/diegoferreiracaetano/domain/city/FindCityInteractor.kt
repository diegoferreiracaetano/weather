package com.diegoferreiracaetano.domain.city

import com.diegoferreiracaetano.domain.Interactor

class FindCityInteractor(
    private val repository: CityRepository
) : Interactor<Int, City>() {

    override fun execute(parameters: Int) = repository.city(parameters)
}
