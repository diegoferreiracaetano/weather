package com.diegoferreiracaetano.domain.city

import com.diegoferreiracaetano.domain.location.Location
import kotlinx.coroutines.flow.Flow

interface CityRepository {
    fun cities(location: Location): Flow<List<City>>
    fun city(id: Int): Flow<City>
}
