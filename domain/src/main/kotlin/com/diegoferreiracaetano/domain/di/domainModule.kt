package com.diegoferreiracaetano.domain.di

import com.diegoferreiracaetano.domain.city.CitiesInteractor
import com.diegoferreiracaetano.domain.city.FindCityInteractor
import com.diegoferreiracaetano.domain.location.LocationInteractor
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

val domainModule: Module = module {
    single {
        CitiesInteractor(
            get(),
            get(named("detail"))
        )
    }

    single {
        FindCityInteractor(
            get()
        )
    }

    single {
        LocationInteractor(
            get()
        )
    }
}
