package com.diegoferreiracaetano.domain.location

import kotlinx.coroutines.flow.Flow

interface LocationRepository {
    fun location(): Flow<Location>
}
