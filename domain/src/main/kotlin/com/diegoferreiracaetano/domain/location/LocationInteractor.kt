package com.diegoferreiracaetano.domain.location

import com.diegoferreiracaetano.domain.Interactor

class LocationInteractor(
    private val repository: LocationRepository
) : Interactor<Unit, Location>() {

    override fun execute(parameters: Unit) = repository.location()
}
