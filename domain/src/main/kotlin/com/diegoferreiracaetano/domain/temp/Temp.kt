package com.diegoferreiracaetano.domain.temp

data class Temp(
    val temp: Double,
    val min: Double,
    val max: Double
)
