package com.diegoferreiracaetano.domain

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

abstract class Interactor<P, R> {

    @ExperimentalCoroutinesApi
    operator fun invoke(parameters: P, dispatcher: CoroutineDispatcher = Dispatchers.IO): Flow<Result<R>> {
        return flow {
            try {
                execute(parameters)
                    .flowOn(dispatcher)
                    .collect { emit(Result.success(it)) }
            } catch (t: Throwable) {
                emit(Result.failure(t))
            }
        }
    }

    protected abstract fun execute(parameters: P): Flow<R>
}
