package com.diegoferreiracaetano.domain.city

import com.diegoferreiracaetano.domain.temp.Temp
import com.diegoferreiracaetano.domain.weather.Weather
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class FindCityInteractorTest {

    private val repository = mockk<CityRepository>()
    private lateinit var interactor: FindCityInteractor

    @Before
    fun setUp() {
        interactor = FindCityInteractor(
            repository
        )
    }

    @Test
    fun `Given repository, When calling city, Then assert values`() {

        runBlocking {

            coEvery { repository.city(1) } returns flowOf(city)

            val result = interactor(1).single().getOrNull()

            assertEquals(result, city)
        }
    }

    val city = City(
        id = 1,
        name = "",
        temp = Temp(
            51.1,
            51.1,
            51.1
        ),
        weather = listOf(
            Weather(
                id = 1,
                main = "",
                description = "",
                icon = null
            )
        )
    )

    val cities = listOf(city)
}
