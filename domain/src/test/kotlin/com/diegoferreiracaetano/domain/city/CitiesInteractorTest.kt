package com.diegoferreiracaetano.domain.city

import com.diegoferreiracaetano.domain.ResultRouter
import com.diegoferreiracaetano.domain.location.Location
import com.diegoferreiracaetano.domain.temp.Temp
import com.diegoferreiracaetano.domain.weather.Weather
import com.diegoferreiracaetano.router.Weather.ListRouter
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class CitiesInteractorTest {

    private val repository = mockk<CityRepository>()
    private val router = mockk<ListRouter>()
    private lateinit var interactor: CitiesInteractor

    @Before
    fun setUp() {
        interactor = CitiesInteractor(
            repository,
            router
        )
    }

    @Test
    fun `Given repository, When calling cities, Then assert values`() {

        runBlocking {

            coEvery { repository.cities(Location(1.0, 1.0)) } returns flowOf(cities)

            val result = interactor(Location(1.0, 1.0)).single().getOrNull()

            assertEquals(result, ResultRouter.add(cities, router))
        }
    }

    val city = City(
        id = 1,
        name = "",
        temp = Temp(
            51.1,
            51.1,
            51.1
        ),
        weather = listOf(
            Weather(
                id = 1,
                main = "",
                description = "",
                icon = null
            )
        )
    )

    val cities = listOf(city)
}
