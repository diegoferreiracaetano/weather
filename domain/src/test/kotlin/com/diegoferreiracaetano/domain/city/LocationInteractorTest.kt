package com.diegoferreiracaetano.domain.city

import com.diegoferreiracaetano.domain.location.Location
import com.diegoferreiracaetano.domain.location.LocationInteractor
import com.diegoferreiracaetano.domain.location.LocationRepository
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

internal class LocationInteractorTest {

    private val repository = mockk<LocationRepository>()
    private lateinit var interactor: LocationInteractor

    @Before
    fun setUp() {
        interactor = LocationInteractor(
            repository
        )
    }

    @Test
    fun `Given repository, When calling location, Then assert values`() {

        runBlocking {

            coEvery { repository.location() } returns flowOf(location)

            val result = interactor(Unit).single().getOrNull()

            assertEquals(result, location)
        }
    }

    private val location = Location(1.0, 1.0)
}
